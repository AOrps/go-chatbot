# Go Chatbot

Server + Chatbot in Golang!

## Technical Details
- Uses Rosharian Psychology, NLP and Sentiment Analysis 


## 



## Usage
Use: 

`nc localhost 8878`

This will allow you connect to the server!


## Next Steps
- Integrate: 
    - [NLP](https://github.com/jdkato/prose)
    - [Sentiment Analysis](https://github.com/cdipaolo/sentiment)