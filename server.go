package gochatbot

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"strings"
)

const (
	HOST   = "localhost"
	PORT   = "8878"
	N_TYPE = "tcp"
)

// func main() {
// 	Serve()
// }

// Might have a corruption attack if you use ^[[B, ^[[A (Up and Down Arrow)
func handleClientRequest(con net.Conn, id int) {
	log.Printf("Client %d connected \n", id)
	defer con.Close()

	clientReader := bufio.NewReader(con)

	for {
		// Waiting for the client request
		clientRequest, err := clientReader.ReadString('\n')

		switch err {
		case nil:
			clientRequest := strings.TrimSpace(clientRequest)

			// Prints out what the Client has said!
			fmt.Printf("Client %d: %s\n", id, clientRequest)
		case io.EOF:
			log.Printf("Client %d: client closed the connection by terminating the process\n", id)
			return
		default:
			log.Printf("error: %v\n", err)
			return
		}

		// Responding to the client request
		if _, err = con.Write([]byte("GOT IT!\n")); err != nil {
			log.Printf("failed to respond to client: %v\n", err)
		}
	}
}

func Serve() {

	// Initialize counter and listener
	num := 0
	listener, err := net.Listen(N_TYPE, HOST+":"+PORT)

	if err != nil {
		log.Fatalln(err)
	}
	defer listener.Close()

	// Handle all TCP Connections Concurrently and Identifies Client via Num
	for {
		con, err := listener.Accept()
		if err != nil {
			log.Println(err)
			continue
		}

		num += 1
		con.Write([]byte("Connected to Chatbot!\n"))
		go handleClientRequest(con, num)
	}
}
